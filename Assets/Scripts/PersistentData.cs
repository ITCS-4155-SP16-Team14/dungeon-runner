﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PersistentData : MonoBehaviour {
	public uint targetHeartRate;

	void Start () {
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Object.DontDestroyOnLoad (this);
	}
}
