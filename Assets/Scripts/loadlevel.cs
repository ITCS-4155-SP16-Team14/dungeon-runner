﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class loadlevel : MonoBehaviour {
	// Use this for initialization
	public void LoadStage () {
		Debug.Log("game object name: " + this.name);
		GameObject obj = GameObject.Find ("PersistentGameObject");
		PersistentData data = obj.GetComponent<PersistentData>();
		if (gameObject.name == "FatburnerCollider") {
			data.targetHeartRate = 130;
		} else if (gameObject.name == "TestModeCollider") {
			
			data.targetHeartRate = 70;
		} else {
			data.targetHeartRate = 160;
		}

		SceneManager.LoadScene ("level_1");
	}
}
	