﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour {
	public Text heartRateText;
	public Text targetHR;
	public float stam = 10;
	public float maxStam = 10;
	bool isRunning;
	private HeartRatePluginDriver heartRateScript;
	private PersistentData persistentData;

	Rect stamRect;
	Texture2D stamTexture;

	// Use this for initialization
	void Start () {
		
		GameObject heartRateMon = GameObject.Find ("BluetoothLEPlugin");
		GameObject persistentObj = GameObject.Find ("PersistentGameObject");
//		GameObject troll = GameObject.Find ("Dwarf_Slayer_Prefab");

//		troll.
		heartRateScript = heartRateMon.GetComponent<HeartRatePluginDriver> ();
		persistentData = persistentObj.GetComponent<PersistentData> ();
		stamRect= new Rect(Screen.width/10,Screen.height*9/10,
			Screen.width/3, Screen.height/50);
		stamTexture = new Texture2D (1, 1);	
		stamTexture.SetPixel(0,0,Color.blue);
		stamTexture.Apply();
		
		targetHR.text =  "Target: " + persistentData.targetHeartRate;
	}

	void SetRunning(bool isRunning){
		this.isRunning = isRunning;
	}

	// Update is called once per frame
	void Update () {
		heartRateText.text = "HR: " + heartRateScript.m_currentrate + "bpm";
		if (heartRateScript.m_currentrate >= persistentData.targetHeartRate + 10)
			SetRunning (true);
		else
			SetRunning (false);

		if (isRunning) 
		{
			stam -= Time.deltaTime;

			if (stam < 0) 
			{
				stam = 0;
				SetRunning (false);
			}
		}
		else if (stam < maxStam) {

			stam += Time.deltaTime;

		}
	
	
	}

	void OnGUI(){
		float ratio = stam / maxStam;
		float rectWidth = ratio * Screen.width / 3;
		stamRect.width = rectWidth;
		GUI.DrawTexture (stamRect, stamTexture);
	}
}
