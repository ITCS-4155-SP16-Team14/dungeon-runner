﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class timer : MonoBehaviour {

	public Text timerText;
	public float totalRunTime;
	public bool finished = false;
	StaminaBar stamina;
	// Use this for initialization
	void Start () {
		stamina = GameObject.Find ("FPSController").GetComponent<StaminaBar>();
		totalRunTime = 600;
	}
	
	// Update is called once per frame
	void Update () {

		if (finished)
			return;
		
		float t = totalRunTime - Time.time;

		string minutes = ((int) t / 60).ToString();
		string seconds = (t % 60).ToString ("f2");
	
		timerText.text = minutes + ":" + seconds;

		if(t <= 0) {
			if (stamina.stam > 0) {
				finished = true;
				SceneManager.LoadScene ("GameOver");
			}
		}
	}

	public void Finish()
	{
		finished = true;
		timerText.color = Color.red;
	}
}
