﻿using UnityEngine;
using UnityStandardAssets;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using System.Collections;
using System.Collections.Generic;

public class PlatformManager : MonoBehaviour {

	public int numberOfPlatforms = 15;
	public List<GameObject> platforms = new List<GameObject> ();

	public Vector3 minScale = new Vector3();
	public Vector3 maxScale = new Vector3();
	private GameObject platformPrefab;
	private Transform player;

	void Start() {
		platformPrefab = Resources.Load ("Prefabs/CORRIDOR_04") as GameObject;
		player = GameObject.Find ("FPSController").transform;

		GameObject g = Instantiate (platformPrefab, Vector3.zero, Quaternion.identity) as GameObject;
	}
	void Update () {
		Vector3 playerPosition = player.position;

		if (platforms.Count < numberOfPlatforms) {
			for (int i = 0; i < numberOfPlatforms; i++) {
				GameObject g = Instantiate (platformPrefab, Vector3.zero, Quaternion.identity) as GameObject;

				Vector3 tempPos = new Vector3 ();
				if (platforms.Count > 0) {
					tempPos = platforms [platforms .Count - 1].transform.position;
					tempPos.x += platforms [platforms.Count - 1].transform.localScale.x * 2f + g.transform.localScale.x * 2f;
				}
				g.transform.position = tempPos;
				g.transform.parent = gameObject.transform;
				platforms.Add (g);
			}
		} else if (platforms [platforms.Count - numberOfPlatforms/2].transform.position.x <= playerPosition.x) {
//			&& playerPosition.x > platforms [platforms.Count - 1].transform.position.x){
			print ("platforms count else if: " + platforms.Count);
			GameObject removedObject = platforms [0];
			platforms.RemoveAt (0);
			Destroy (removedObject);
		}
	}
}
