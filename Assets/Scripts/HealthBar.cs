﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class HealthBar : MonoBehaviour {
	public float health, maxHealth;
	private HeartRatePluginDriver heartRateScript;
	private PersistentData persistentData;

	Rect healthRect;
	Texture2D healthTexture;

	// Use this for initialization
	void Start () {
		GameObject heartRateMon = GameObject.Find ("BluetoothLEPlugin");
		GameObject persistentObj = GameObject.Find ("PersistentGameObject");

		heartRateScript = heartRateMon.GetComponent<HeartRatePluginDriver> ();
		persistentData = persistentObj.GetComponent<PersistentData> ();
		healthRect= new Rect(Screen.width/10,Screen.height*8/10,
			Screen.width/3, Screen.height/50);
		healthTexture = new Texture2D (1, 1);
		healthTexture.SetPixel(0,0,Color.green);
		healthTexture.Apply();

	}

	// Update is called once per frame
	void Update () {
		if (health < 0) 
		{
			health = 0;
			SceneManager.LoadScene ("GameOver");
		}
	}

	void OnGUI(){
		float ratio = health / maxHealth;
		float rectWidth = ratio * Screen.width / 3;
		healthRect.width = rectWidth;
		GUI.DrawTexture (healthRect, healthTexture);
	}
}
