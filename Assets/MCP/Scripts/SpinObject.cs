using UnityEngine;
using System.Collections;

public class SpinObject : MonoBehaviour
{
	Transform trans;
	
	public bool xAxis = false;
	public float xSpeed = 1f;
	public bool yAxis = true;
	public float ySpeed = 1f;
	public bool zAxis = false;
	public float zSpeed = 1f;
	
	Vector3 eulers;
	
	void Update()
	{
		if( this.trans == null ){ this.trans = this.transform; this.eulers = this.trans.eulerAngles; }
		
		if( this.xAxis == true ){ this.eulers.x += this.xSpeed *Time.deltaTime; }
		if( this.yAxis == true ){ this.eulers.y += this.ySpeed *Time.deltaTime; }
		if( this.zAxis == true ){ this.eulers.z += this.zSpeed *Time.deltaTime; }
		this.trans.eulerAngles = this.eulers;
	}
}
