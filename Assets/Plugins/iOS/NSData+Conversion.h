//
//  NSData+Conversion.h
//  ble_plugin
//
//  Created by Michael Hoffstaedter on 04.02.14.
//  Copyright (c) 2014 Michael Hoffstaedter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (NSData_Conversion)

#pragma mark - String conversion
- (NSString *)hexadecimalString;

@end
