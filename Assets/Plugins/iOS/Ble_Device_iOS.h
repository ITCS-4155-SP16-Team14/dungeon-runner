//
//  BluetoothLEPluginiOS
//  BluetoothLEPluginiOS
//
//  Created by Alex Kern on 11.03.14.
//  Copyright (c) 2014 Alex Kern. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "NSData+Conversion.h"

@interface BluetoothLEPluginiOS : NSObject <CBPeripheralDelegate, CBCentralManagerDelegate>{
    BOOL                Logging;
    CBCentralManager    *manager;
    CBPeripheral        *peripheral;
    NSMutableArray      *ScanServices;
    NSMutableArray      *BleDeviceArr;//*heartRateMonitors;
    NSMutableData       *currentResponse;
    int                 dataCounter;
    BOOL                shouldIncludeDataCount;
    BOOL                bScanning;
    NSString            *currentPrefix;
    NSTimeInterval      lastData;
    NSData              *currentCommand;
    int                 devicecounter; //workaround for ios 6
    
    CBCharacteristic    *HRM_Measurement;
    CBCharacteristic    *HRM_ControlPoint;
    CBCharacteristic    *HRM_BodySensorLocation;
}

-(void)InitPluginWithLogging:(BOOL)log;
@end

NSString        *assemblyPath;

#define UUID_HEART_RATE_SERV                @"180d"
#define UUID_HEART_RATE_MEASUREMENT_CHAR    @"2a37"
#define UUID_HRM_BODY_SENSOR_LOCATION_CHAR  @"2a38"
#define UUID_HRM_CONTROL_POINT_CHAR         @"2a39"
#define UUID_CLIENT_CHARACTERISTIC_CONFIG   @"2902"
