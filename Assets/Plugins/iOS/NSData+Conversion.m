//
//  NSData+Conversion.m
//  ble_plugin
//
//  Created by Michael Hoffstaedter on 04.02.14.
//  Copyright (c) 2014 Michael Hoffstaedter. All rights reserved.
//

#import "NSData+Conversion.h"

@implementation NSData (NSData_Conversion)

#pragma mark - String Conversion
- (NSString *) hexadecimalString {
    const unsigned char *buf = (const unsigned char *)[self bytes];
    if (!buf) {
        return [NSString string];
    }
    
    NSUInteger len = [self length];
    NSMutableString *hex = [NSMutableString stringWithCapacity:(len*2)];
    for (int i = 0; i < len; i++) {
        [hex appendString:[NSString stringWithFormat:@"%02lx ", (unsigned long)buf[i]]];
    }
    return [NSString stringWithString:hex];
}

@end
