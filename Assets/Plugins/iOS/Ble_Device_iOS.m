//
//  BluetoothLEPluginiOS.m
//  BluetoothLEPluginiOS
//
//  Created by Alex Kern on 11.03.14.
//  Copyright (c) 2014 Alex Kern. All rights reserved.
//

#import "Ble_Device_iOS.h"
#import <Foundation/NSUUID.h>

@implementation BluetoothLEPluginiOS

NSString        *TAG = @"Ble_Device_iOS: ";

- (void)InitPluginWithLogging:(BOOL)log {
    Logging = log;
    bScanning   = false;
    manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    //if Bluetooth is powered off: Userrequest to turn it on
    
}

- (void) dealloc {
    [self stopScan];
    [peripheral setDelegate:nil];
}

-(void) stopPlugin {
    if (peripheral) {
        [manager cancelPeripheralConnection:peripheral];
    }
}

- (void) disconnect {
    [self disconnectWithReason:@"8"];
}

- (void) disconnectReason:(NSString *)reason {
    if (Logging)
        NSLog(@"%@Disconnecting because of user interaction",TAG);
    if (bScanning)
        [self stopScan];
    if (peripheral) {
        [manager cancelPeripheralConnection:peripheral];
    }
    if (BleDeviceArr != nil)
        [BleDeviceArr removeAllObjects];
    if (ScanServices != nil)
        [ScanServices removeAllObjects];
    if( peripheral) {
        peripheral = nil;
        HRM_Measurement = nil;
        HRM_ControlPoint = nil;
        HRM_BodySensorLocation = nil;
    }
    if (Logging)
        NSLog(@"%@peripheral and Characteristics resetted",TAG);
    UnitySendMessage("BluetoothLEPlugin","ReportDisconnect",[reason cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void) disconnectWithReason:(NSString *)reason {
    if (peripheral) {
        [manager cancelPeripheralConnection:peripheral];
    }
    UnitySendMessage("BluetoothLEPlugin","ReportDisconnect",[reason cStringUsingEncoding:NSUTF8StringEncoding]);
}

#pragma mark - Device Data

- (int) GetManagerState {
    if (Logging)
        NSLog(@"%@GetManagerState is: %d",TAG,(int)[manager state]);
    return (int)[manager state];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSString *StateString = [NSString stringWithFormat:@"%i", central.state];
    NSLog(@"%@centralManagerDidUpdateState to: %@",TAG, StateString);
    UnitySendMessage("BluetoothLEPlugin","ReportactualBleState",[StateString cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void) EnableBluetoothManager {
    NSLog(@"%@EnableBluetoothManager called",TAG);
    manager = NULL;
    manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    //manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionShowPowerAlertKey:[NSNumber numberWithBool:YES]}];
}

- (void) startScan:(NSString *)ScanForDevice stopAfter:(int)seconds {
    devicecounter = 0;
    if (seconds == 0)
        seconds = 60;
    if (bScanning)
        [self stopScan];
    ScanServices = [[NSMutableArray alloc] init];
    if (ScanServices != nil)
        [ScanServices removeAllObjects];
    if (BleDeviceArr != nil)
        [BleDeviceArr removeAllObjects];
    if (Logging){
        NSLog(@"%@Scantime: %d seconds",TAG,seconds);
        NSLog(@"%@ScanForDevice: %@",TAG,ScanForDevice);
    }
    if ([ScanForDevice rangeOfString:@"180d"].location != NSNotFound){
        if(Logging)
            NSLog(@"%@HeartRate-service selected",TAG);
        [ScanServices addObject:@"180d"];
    }
    bScanning = true;
    [manager scanForPeripheralsWithServices:nil options:nil];
    
    [self performSelector:@selector(stopScan) withObject:nil afterDelay:seconds];
}

- (void) stopScan {
    if(bScanning){
        if (Logging)
            NSLog(@"%@Stopping Scan",TAG);
        [manager stopScan];
        bScanning = false;
    }
}

- (void) Connect:(NSString *)identifier {
    if (Logging)
        NSLog(@"%@Got connect-command with: %@",TAG, identifier);
    if (bScanning)
        [self stopScan];
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        for(NSArray *compPeri in BleDeviceArr){
            CBPeripheral *peri = [compPeri objectAtIndex: 0];
            if ([[[peri identifier]UUIDString]isEqualToString: identifier]) {
                [manager connectPeripheral: peri options: [NSDictionary dictionaryWithObject: [NSNumber numberWithBool:YES] forKey: CBConnectPeripheralOptionNotifyOnDisconnectionKey]];
                return;
            }
        }
    } else {
        [manager connectPeripheral: [[BleDeviceArr objectAtIndex: [identifier intValue]] objectAtIndex: 0] options: [NSDictionary dictionaryWithObject: [NSNumber numberWithBool: YES] forKey: CBConnectPeripheralOptionNotifyOnDisconnectionKey]];
        return;
    }
    UnitySendMessage("BluetoothLEPlugin","ReportDisconnect","6");
}

#pragma mark - CBCentralManagerDelegate
- (NSString *) stringFromCBUUID:(CBUUID *)theid {
    NSData *data = [theid data];
    NSUInteger bytesToConvert = [data length];
    const unsigned char *uuidBytes = [data bytes];
    NSMutableString *outputString = [NSMutableString stringWithCapacity: 16];
    for (NSUInteger currentByteIndex = 0; currentByteIndex < bytesToConvert; currentByteIndex++){
        switch (currentByteIndex){
            case 3:
            case 5:
            case 7:
            case 9: [outputString appendFormat: @"%02x-", uuidBytes[currentByteIndex]]; break;
            default: [outputString appendFormat: @"%02x", uuidBytes[currentByteIndex]];
        }
    }
    return outputString;
}
- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)aPeripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    if (Logging){
        NSLog(@"\n\n                                        %@*************** Device found ***************\n",TAG);
        NSLog(@"%@Identifier         : %@",TAG, aPeripheral.identifier.UUIDString);
        NSLog(@"%@Name               : %@",TAG, aPeripheral.name);
        NSLog(@"%@RSSI               : %@",TAG, RSSI);
    }
    NSString *devicetype                = @"";
    NSMutableArray *FinalPeripherals    = [self mutableArrayValueForKey: @"BleDeviceArr"];//creates a mutable copy of BleDeviceArr
    NSMutableArray *CompPeripherals;
    CompPeripherals = [[NSMutableArray alloc] init];
    if (BleDeviceArr != nil){
        for(NSArray *compPeri in BleDeviceArr){
            //creates a copy of BleDeviceArr with only the Peripherals:
            [CompPeripherals addObject: [compPeri objectAtIndex: 0]];
        }
    }
    //Peripheral discovered for the first time:
    if (![CompPeripherals containsObject: aPeripheral]){
        NSString *deviceServUUID    = @"";
        NSString *peripheralName    = @"";
        //UUID from AdvData
        if ([advertisementData objectForKey: CBAdvertisementDataServiceUUIDsKey] != nil){
            for (CBUUID *uuid in [advertisementData objectForKey: CBAdvertisementDataServiceUUIDsKey]) {
                deviceServUUID = [self stringFromCBUUID: uuid];
            }
            //Name Placeholder
            if ([deviceServUUID isEqualToString: @"180d"]){
                peripheralName = @"HeartRate";
                devicetype = @"1";
            }
        }
        //Name from AdvKey
        if ([advertisementData objectForKey: CBAdvertisementDataLocalNameKey] != nil) {
            peripheralName = [advertisementData objectForKey: CBAdvertisementDataLocalNameKey];
        }
        //"real" Name
        if (aPeripheral.name != nil){
            peripheralName = aPeripheral.name;
        }
        NSArray *peripheralContent = [NSArray arrayWithObjects: aPeripheral, [NSNumber numberWithInteger: devicecounter], deviceServUUID, peripheralName, @"no", nil];
        [FinalPeripherals addObject: peripheralContent];
        // all needed Data is available
        if (![deviceServUUID isEqualToString: @""] && ![peripheralName isEqualToString: @""]){
            // deviceServUUID is in ScanList
            if ([deviceServUUID rangeOfString: @"180d"].location != NSNotFound){
                peripheralContent = [NSArray arrayWithObjects: aPeripheral, [NSNumber numberWithInteger: devicecounter], deviceServUUID, peripheralName, @"yes", nil];
                [FinalPeripherals replaceObjectAtIndex: devicecounter withObject: peripheralContent];
                NSString *sendString;
                if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
                    sendString = [NSString stringWithFormat: @"%@,%@,%@,%@", aPeripheral.identifier.UUIDString, devicetype, peripheralName, deviceServUUID];
                } else {
                    sendString = [NSString stringWithFormat: @"%d,%@,%@,%@", devicecounter, devicetype, peripheralName, deviceServUUID];
                }
                UnitySendMessage("BluetoothLEPlugin","ReportMacadressFound",[sendString cStringUsingEncoding: NSUTF8StringEncoding]);
            }
        }
        devicecounter++;
    } else {//Peripheral discovered again
        if (Logging)
            NSLog(@"%@Device already scanned",TAG);
        int existingActualDevicecounter = 0;
        existingActualDevicecounter = [CompPeripherals indexOfObject: aPeripheral];
        //if the actual aPeripheral was not already sent
        if (![[[FinalPeripherals objectAtIndex: existingActualDevicecounter] objectAtIndex: 4] isEqualToString: @"yes"]){
            NSString *deviceServUUID    = [[FinalPeripherals objectAtIndex: existingActualDevicecounter] objectAtIndex: 2];//should always be @""
            NSString *peripheralName    = [[FinalPeripherals objectAtIndex: existingActualDevicecounter] objectAtIndex: 4];
            //UUID from AdvData
            if ([advertisementData objectForKey: CBAdvertisementDataServiceUUIDsKey] != nil){
                for (CBUUID *uuid in [advertisementData objectForKey: CBAdvertisementDataServiceUUIDsKey]) {
                    deviceServUUID = [self stringFromCBUUID: uuid];
                }
                //Name Placeholder
                if ([deviceServUUID isEqualToString: @"180d"]){
                    peripheralName = @"HeartRate";
                    devicetype = @"1";
                }
            }
            //Name from AdvKey
            if ([advertisementData objectForKey: CBAdvertisementDataLocalNameKey] != nil) {
                peripheralName = [advertisementData objectForKey: CBAdvertisementDataLocalNameKey];
            }
            //"real" Name
            if (aPeripheral.name != nil){
                peripheralName = aPeripheral.name;
            }
            NSArray *peripheralContent = [NSArray arrayWithObjects: aPeripheral, [NSNumber numberWithInteger: existingActualDevicecounter], deviceServUUID, peripheralName, @"no", nil];
            [FinalPeripherals replaceObjectAtIndex: existingActualDevicecounter withObject: peripheralContent];
            // all needed Data is available
            if (![deviceServUUID isEqualToString: @""] && ![peripheralName isEqualToString: @""]){
                // deviceServUUID is in ScanList
                if ([deviceServUUID rangeOfString: @"180d"].location != NSNotFound){
                    peripheralContent = [NSArray arrayWithObjects: aPeripheral, [NSNumber numberWithInteger: existingActualDevicecounter], deviceServUUID, peripheralName, @"yes", nil];
                    [FinalPeripherals replaceObjectAtIndex: existingActualDevicecounter withObject: peripheralContent];
                    NSString *sendString;
                    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
                        sendString = [NSString stringWithFormat: @"%@,%@,%@,%@", aPeripheral.identifier.UUIDString, devicetype, peripheralName, deviceServUUID];
                    } else {
                        sendString = [NSString stringWithFormat: @"%d,%@,%@,%@", existingActualDevicecounter, devicetype, peripheralName, deviceServUUID];
                    }
                    UnitySendMessage("BluetoothLEPlugin","ReportMacadressFound",[sendString cStringUsingEncoding: NSUTF8StringEncoding]);
                }
            }
        } else {
            if (Logging){
                NSLog(@"%@Device already sent",TAG);
            }
        }
    }
    if (Logging){
        NSLog(@"%@*************** End Device found ***************",TAG);
        NSLog(@"\n");
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
    if (Logging)
        NSLog(@"%@Retrieved peripheral: %lu - %@",TAG, (unsigned long)[peripherals count], peripherals);
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)aPeripheral {
    if (Logging)
        NSLog(@"%@did connect to peripheral: %@",TAG,aPeripheral.name);
    [aPeripheral setDelegate:self];
    [aPeripheral discoverServices:nil];
    peripheral = aPeripheral;
    dataCounter = 0;
    UnitySendMessage("BluetoothLEPlugin", "ReportConnect",[@"" cStringUsingEncoding:NSUTF8StringEncoding]);
}

/*
 Invoked whenever an existing connection with the peripheral is torn down.
 Reset local variables
 */
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error {
    if( peripheral ) {
        [peripheral setDelegate:nil];
        peripheral = nil;
        if (ScanServices != nil)
            [ScanServices removeAllObjects];
        HRM_Measurement = nil;
        HRM_ControlPoint = nil;
        HRM_BodySensorLocation = nil;
        if (Logging)
            NSLog(@"%@peripheral and Characteristics resetted",TAG);
    }
    if ([error code]!= 0) {
        NSString *reason = [NSString stringWithFormat:@"%d",[error code]+10];
        if (Logging)
            NSLog(@"%@Gotdisconnect, reason: %@",TAG,reason);
        [self performSelector:@selector(sendDisconnectWithReason:) withObject:reason afterDelay:1.5];
    }
}

- (void) sendDisconnectWithReason:(NSString *)reason {
    UnitySendMessage("BluetoothLEPlugin","ReportDisconnect",[reason cStringUsingEncoding:NSUTF8StringEncoding]);
}

/*
 Invoked whenever the central manager fails to create a connection with the peripheral.
 */
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)aPeripheral error:(NSError *)error {
    NSLog(@"%@Fail to connect to peripheral: %@ with error = %@",TAG, aPeripheral, [error localizedDescription]);
    if( peripheral ) {
        [peripheral setDelegate:nil];
        peripheral = nil;
    }
}

#pragma mark - CBPeripheral delegate methods
/*
 Invoked upon completion of a -[discoverServices:] request.
 Discover available characteristics on interested services
 */
- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverServices:(NSError *)error {
    for (CBService *aService in aPeripheral.services) {
        if (Logging)
            NSLog(@"%@didDiscoverServices with UUID: %@",TAG,[[aService.UUID data] description]);
        if ([aService.UUID isEqual:[CBUUID UUIDWithString:UUID_HEART_RATE_SERV]]) {
            [aPeripheral discoverCharacteristics:nil forService:aService];
        }
    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    if ([service.UUID isEqual:[CBUUID UUIDWithString:UUID_HEART_RATE_SERV]]) {
        for (CBCharacteristic *aChar in service.characteristics) {
            if (Logging)
                NSLog(@"%@didDiscoverCharacteristicsForService HeartRate with UUID: %@",TAG,[[aChar.UUID data]description]);
            // Set Characteristic for heart rate measurement
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUID_HEART_RATE_MEASUREMENT_CHAR]]) {
                HRM_Measurement = aChar;
                NSLog(@"%@HRM_Measurement set",TAG);
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(HrmReadyForSync) object:nil];
                [self performSelector:@selector(HrmReadyForSync) withObject:nil afterDelay:0.150];
            }
            // Set Characteristic for body sensor location
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUID_HRM_BODY_SENSOR_LOCATION_CHAR]]) {
                HRM_BodySensorLocation = aChar;
                NSLog(@"%@BodySensorLocation set",TAG);
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(HrmReadyForSync) object:nil];
                [self performSelector:@selector(HrmReadyForSync) withObject:nil afterDelay:0.150];
            }
            // Set Characteristic for HeartRateControlPoint if featured
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUID_HRM_CONTROL_POINT_CHAR]]) {
                HRM_ControlPoint = aChar;
                UnitySendMessage("BluetoothLEPlugin", "ReportHRControlPointFeatured",[@"" cStringUsingEncoding:NSUTF8StringEncoding]);
            }
        }
    }
}

- (void) peripheral:(CBPeripheral *)aPeripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (Logging)
        NSLog(@"%@Got data: %@",TAG,characteristic.value.description);
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUID_HRM_BODY_SENSOR_LOCATION_CHAR]]) {
        [self PrepareData:characteristic];
        [peripheral setNotifyValue:YES forCharacteristic:HRM_Measurement];
    } else {
        [self PrepareData:characteristic];
    }
}

- (void) peripheral:(CBPeripheral *)aperipheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error != nil)
        NSLog(@"%@Error updating notification state: %@",TAG,[error description]);
}

- (void) peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error != nil) {
        NSLog(@"%@Error writing: %@",TAG,[error description]);
        [self disconnectWithReason:@"5"];
    } else {
        
    }
}

#pragma mark - Timer for HRM
- (void)HrmReadyForSync {
    if (Logging)
        NSLog(@"%@HrmReadyForSync called",TAG);
    UnitySendMessage("BluetoothLEPlugin", "ReadyForSync",[@"" cStringUsingEncoding:NSUTF8StringEncoding]);
}

#pragma mark - Handling Data

- (void) PrepareData:(CBCharacteristic *)aChar {
    currentResponse = [aChar.value mutableCopy];
    if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUID_HEART_RATE_MEASUREMENT_CHAR]]) {
        currentPrefix = @"2a37 ";
        shouldIncludeDataCount = NO;
    } else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:UUID_HRM_BODY_SENSOR_LOCATION_CHAR]]) {
        currentPrefix = @"2a38 ";
        shouldIncludeDataCount = NO;
    } else {
        currentPrefix = @"";
        shouldIncludeDataCount = NO;
    }
    [self SendCurrentData];
}

- (void) SendCurrentData {
    if (Logging)
        NSLog(@"%@Current prefix is: %@",TAG,currentPrefix);
    NSMutableString *aString = [[NSMutableString alloc] initWithString:currentPrefix];
    if (shouldIncludeDataCount) {
        [aString appendFormat:@"%d ",dataCounter];
    }
    [aString appendString:[[currentResponse copy] hexadecimalString]];
    dataCounter = 0;
    currentPrefix = @"";
    shouldIncludeDataCount = NO;
    if (Logging)
        NSLog(@"%@Sending Data to Unity",TAG);
    [currentResponse setLength:0];
    [self performSelector:@selector(SendDelayedWithString:) withObject:aString afterDelay:0.15];
    
}

- (void) SendDelayedWithString:(NSMutableString *)theString {
    UnitySendMessage("BluetoothLEPlugin","LogBleDevice",[[[[NSString alloc] initWithString:theString] uppercaseString]cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void) GetFeatures:(NSString *)device {
    if ([device isEqualToString:@"heartrate"]) {
        if (Logging)
            NSLog(@"%@gotFeatures, reading BodySensorLocation",TAG);
        if (HRM_BodySensorLocation != nil){
            [peripheral readValueForCharacteristic:HRM_BodySensorLocation];
        } else {
            if (Logging)
                NSLog(@"%@BodySensorLocation not featured, setting Notifier for HeartRateMeasurement",TAG);
            [peripheral setNotifyValue:YES forCharacteristic:HRM_Measurement];
        }
    }
}

- (void) RetryCurrentCommandWithReset:(BOOL)resetDataCounter andCharacteristic:(CBCharacteristic *)aChar {
    [currentResponse setLength:0];
    if (resetDataCounter)
        dataCounter = 0;
    if (peripheral != nil) {
        [peripheral writeValue:currentCommand forCharacteristic:aChar type:CBCharacteristicWriteWithResponse];
    }
}

//TODO: ??
- (void) SendBytesToDevice:(uint8_t[])bytes andLength:(int)length andPrefix:(NSString *)prefix andIncludeDataCount:(BOOL)datac andDeviceType:(NSString *)device {
    if (Logging)
        NSLog(@"%@Current prefix is: %@",TAG,prefix);
    CBCharacteristic *mychar;
    if ([device isEqualToString:@"heartrate"]) {
        mychar = HRM_ControlPoint;
    }
    shouldIncludeDataCount = datac;//TODO: ??
    currentPrefix = prefix;//TODO: ??
    currentCommand = [[NSData alloc] initWithBytes:bytes length:length];
    if (Logging)
        NSLog(@"%@Writing data: %@",TAG,[currentCommand description]);
    if (peripheral != nil) {
        [peripheral writeValue:currentCommand forCharacteristic:mychar type:CBCharacteristicWriteWithResponse];
    }
}

@end

BluetoothLEPluginiOS *currentPlugin = nil;

/*
 Methods called by BluetoothLEPlugin.cs
 */
void InitpluginiOS(bool Logging) {
    NSLog(@"%@Intializing",TAG);
    currentPlugin = [BluetoothLEPluginiOS alloc];
    [currentPlugin InitPluginWithLogging:Logging];
}

int GetBLEStatusiOS() {
    return [currentPlugin GetManagerState];
}

void EnableBluetoothiOS() {
    [currentPlugin EnableBluetoothManager];
}

void ScaniOS(char * device, int secondstoscan) {
    NSString *s_Device = [NSString stringWithUTF8String:device];
    [currentPlugin startScan:s_Device stopAfter:secondstoscan];
}

void StopScaniOS() {
    [currentPlugin stopScan];
}

void StopDeviceiOS() {
    [currentPlugin stopPlugin];
}

void ConnectiOS(char * uuid, char *devicetype) {
    [currentPlugin Connect:[NSString stringWithUTF8String:uuid]];
}

void SendBytesToDeviceiOS(uint8_t bytes[], int length, char * prefix, bool includedatacount, char * device) {
    [currentPlugin SendBytesToDevice:bytes andLength:length andPrefix:[NSString stringWithUTF8String:prefix] andIncludeDataCount:includedatacount andDeviceType:[NSString stringWithUTF8String:device]];
}

void AskForFeaturesiOS(char * device) {
    [currentPlugin GetFeatures:[NSString stringWithUTF8String:device]];
}

void DisconnectiOS() {
    [currentPlugin disconnect];
}
void DisconnectWithReasoniOS(char *reason) {
    [currentPlugin disconnectReason:[NSString stringWithUTF8String:reason]];
}
